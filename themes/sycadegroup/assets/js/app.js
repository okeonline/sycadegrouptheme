$(document).ready(function(){
  $('a').each(function() {
       if ( $(this).attr('target') == '_blank') {
           return;
       } else if ( $(this).hasClass('modal') ) {
          return
      } else {
          $(this).addClass('animsition-link');
       };
   });
});

$(function() {

  window.onscroll = function() {myFunction()};

function myFunction() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  var scrolled = (winScroll / height) * 100;
  document.getElementById("scroll-indicator").style.width = scrolled + "%";
}


  function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

$(window).scroll(function () {
    $('.image-marker-animation').each(function () {
        if (isScrolledIntoView(this) === true) {
            $(this).addClass('in-view');
        }
    });

});

$(document).ready(function() {
  //parallax scroll
  $(window).on("load scroll", function() {
    var parallaxElement = $(".image-container > img"),
      parallaxQuantity = parallaxElement.length;
    window.requestAnimationFrame(function() {
      for (var i = 0; i < parallaxQuantity; i++) {
        var currentElement = parallaxElement.eq(i),
          windowTop = $(window).scrollTop(),
          elementTop = currentElement.offset().top,
          elementHeight = currentElement.height(),
          viewPortHeight = window.innerHeight * 0.5 - elementHeight * 0.5,
          scrolled = windowTop - elementTop + viewPortHeight;
        currentElement.css({
          transform: "translate3d(0," + scrolled * -0.15 + "px, 0)"
        });
      }
    });
  });
});

  $(".scroll-down").click(function() {
    var cls = $(this).closest("section").next().offset().top -0;
    $("html, body").animate({scrollTop: cls}, "slow");
  });

    var lFollowX = 0,
    lFollowY = 0,
    x = 0,
    y = 0,
    friction = 1 / 30;

    function moveBackground() {
      x += (lFollowX - x) * friction;
      y += (lFollowY - y) * friction;
      
      translate = 'translate(' + x + 'px, ' + y + 'px) scale(1.1)';

      $('.moving-bg').css({
        '-webit-transform': translate,
        '-moz-transform': translate,
        'transform': translate
      });

      window.requestAnimationFrame(moveBackground);
    }

    $(window).on('mousemove click', function(e) {

      var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
      var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
      lFollowX = (50 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
      lFollowY = (25 * lMouseY) / 100;

    });

    moveBackground();

    $('.hamburger').on('click', function(){
        $(this).toggleClass('open');
        $('.menu-overlay').toggleClass('open'); 
        $('body').toggleClass('overlay-is-open');
    });

    $('.client-carousel-list').slick({
      arrows: false,
      dots: false,
      autoplay: true,
      // autoplaySpeed: 3000,
      slidesToShow: 5,
      slidesToScroll: 5,
      focusOnSelect: true,
      pauseOnHover:true,
      autoplaySpeed: 0,
      cssEase: 'linear',
      speed: 30000,
      infinite: true,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 4,
          }
        },
        {
          breakpoint: 910,
          settings: {
            slidesToShow: 3,
          }
        },
        {
          breakpoint: 500,
          settings: {
            slidesToShow: 2,
          }
        }
      ]
    });

    $('.products-list').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: false,
        responsive: [
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 2,
            }
          },
          {
            breakpoint: 910,
            settings: {
              slidesToShow: 2,
            }
          },
          {
            breakpoint: 500,
            settings: {
              slidesToShow: 1,
            }
          }
        ]
    });

    $('.stories-list').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: false,
        responsive: [
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 2,
            }
          },
          {
            breakpoint: 910,
            settings: {
              slidesToShow: 2,
            }
          },
          {
            breakpoint: 500,
            settings: {
              slidesToShow: 1,
            }
          }
        ]
    });

    var $status = $('.slide-info');
    var $slickElement = $('.cases-list');

    $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
      //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
      if(!slick.$dots){
        return;
      }
      
      var i = (currentSlide ? currentSlide : 0) + 1;
      $status.text(i + '/' + (slick.$dots[0].children.length));
    });

    $('.cases-list').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      arrows: true,
      prevArrow: "<span class='slick-prev'><i class='fal fa-arrow-left'></i></span>",
      nextArrow: "<span class='slick-next'><i class='fal fa-arrow-right'></i></span>"
  });


  $('.image-carousel-intro .image-list').slick({
    arrows: false,
    dots: false,
    autoplay: true,
    // autoplaySpeed: 3000,
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplaySpeed: 0,
    cssEase: 'linear',
    speed: 30000,
    infinite: true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 910,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  });

  var $status = $('.slide-info');
  var $slickElement = $('.values-list');

  $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
    if(!slick.$dots){
      return;
    }
    
    var i = (currentSlide ? currentSlide : 0) + 1;
    $status.text(i + '/' + (slick.$dots[0].children.length));
  });

  $('.values-list').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: true,
    prevArrow: "<span class='slick-prev'><i class='fal fa-arrow-left'></i></span>",
    nextArrow: "<span class='slick-next'><i class='fal fa-arrow-right'></i></span>"
});

$('.team-list').slick({
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows: true,
  prevArrow: "<span class='slick-prev'><i class='fal fa-arrow-left'></i></span>",
  nextArrow: "<span class='slick-next'><i class='fal fa-arrow-right'></i></span>",
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 910,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
      }
    }
  ]
});

$('.culture-carousel .image-list').slick({
  arrows: false,
  dots: false,
  autoplay: true,
  // autoplaySpeed: 3000,
  slidesToShow: 3,
  slidesToScroll: 3,
  autoplaySpeed: 0,
  cssEase: 'linear',
  speed: 30000,
  infinite: true,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 910,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
      }
    }
  ]
});

$('.expertise-quote-image-carousel .image-list').slick({
  arrows: false,
  dots: false,
  autoplay: true,
  // autoplaySpeed: 3000,
  slidesToShow: 3,
  slidesToScroll: 3,
  autoplaySpeed: 0,
  cssEase: 'linear',
  speed: 30000,
  infinite: true,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 910,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
      }
    }
  ]
});

$('.expertise-quote-image-carousel .quote-carousel').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  dots: false,
  arrows: true,
  prevArrow: "<span class='slick-prev'><i class='fal fa-arrow-left'></i></span>",
  nextArrow: "<span class='slick-next'><i class='fal fa-arrow-right'></i></span>"
});

$('.cases-detail-gallery-quote .image-list').slick({
  arrows: false,
  dots: false,
  autoplay: true,
  // autoplaySpeed: 3000,
  slidesToShow: 3,
  slidesToScroll: 3,
  autoplaySpeed: 0,
  cssEase: 'linear',
  speed: 30000,
  infinite: true
});

$('.cases-detail-gallery-quote .quote-carousel').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  dots: false,
  arrows: true,
  prevArrow: "<span class='slick-prev'><i class='fal fa-arrow-left'></i></span>",
  nextArrow: "<span class='slick-next'><i class='fal fa-arrow-right'></i></span>"
});


});


/* Animated pointer */

const pointer = document.createElement("div")
pointer.id = "pointer-dot"
const ring = document.createElement("div")
ring.id = "pointer-ring"
document.body.insertBefore(pointer, document.body.children[0])
document.body.insertBefore(ring, document.body.children[0])

let mouseX = -100
let mouseY = -100
let ringX = -100
let ringY = -100
let isHover = false
let mouseDown = false
const init_pointer = (options) => {

    window.onmousemove = (mouse) => {
        mouseX = mouse.clientX
        mouseY = mouse.clientY
    }

    window.onmousedown = (mouse) => {
        mouseDown = true
    }

    window.onmouseup = (mouse) => {
        mouseDown = false
    }

    const trace = (a, b, n) => {
        return (1 - n) * a + n * b;
    }
    window["trace"] = trace

    const getOption = (option) => {
        let defaultObj = {
            // pointerColor: "#750c7e",
            ringSize: 15,
            ringClickSize: (options["ringSize"] || 15) - 5,
        }
        if (options[option] == undefined) {
            return defaultObj[option]
        } else {
            return options[option]
        }
    }

    const render = () => {
        ringX = trace(ringX, mouseX, 0.2)
        ringY = trace(ringY, mouseY, 0.2)

        if (document.querySelector(".p-action-click:hover")) {
            pointer.style.borderColor = getOption("pointerColor")
            isHover = true
        } else {
            // pointer.style.borderColor = "white"
            isHover = false
        }
        ring.style.borderColor = getOption("pointerColor")
        if (mouseDown) {
            ring.style.padding = getOption("ringClickSize") + "px"
        } else {
            ring.style.padding = getOption("ringSize") + "px"
        }

        pointer.style.transform = `translate(${mouseX}px, ${mouseY}px)`
        ring.style.transform = `translate(${ringX - (mouseDown ? getOption("ringClickSize") : getOption("ringSize"))}px, ${ringY - (mouseDown ? getOption("ringClickSize") : getOption("ringSize"))}px)`

        requestAnimationFrame(render)
    }
    requestAnimationFrame(render)
}


$(document).ready(function() {
  var a = 0;
  $(window).scroll(function() {
    if($('.counter-box').length>0){	
    var oTop = $('.counter-box').offset().top - window.innerHeight;
    if (a == 0 && $(window).scrollTop() > oTop) {
      $('.counter').each(function() {
        var $this = $(this),
          countTo = $this.attr('data-count');
        $({
          countNum: $this.text()
        }).animate({
            countNum: countTo
          },

          {

            duration: 2000,
            easing: 'swing',
            step: function() {
              $this.text(Math.floor(this.countNum));
            },
            complete: function() {
              $this.text(this.countNum);
              //alert('finished');
            }

          });
      });
      a = 1;
    }
}
  });
});
